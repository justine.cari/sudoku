package sudoku;

import java.util.ArrayList;
import java.util.List;

public class Line {
	
	private int index;
	private List<Cell> cellList = new ArrayList<>();
	
	public Line (int index, List<Cell> cellList)
	{
		this.index = index;
		
		for (Cell cell : cellList) {
			if (cell.getLine() == index)
			{
				this.cellList.add(cell);
			}
		}
		
		
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public List<Cell> getCellList() {
		return cellList;
	}

	public void setCellList(List<Cell> cellList) {
		this.cellList = cellList;
	}
}

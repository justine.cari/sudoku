package sudoku;

import java.util.ArrayList;
import java.util.List;

public class Area {

	public int id;
	public int index; 
	public List<Cell> cellList = new ArrayList<>();
	
	public Area (int index, List<Cell> cellList)
	{
		this.index = index;
		
		for (Cell cell : cellList) {
			if (cell.getArea() == index)
			{
				this.cellList.add(cell);
			}
		}		
	}
	
	public List<Cell> getCellList() {
		return cellList;
	}

	public void setCellList(List<Cell> cellList) {
		this.cellList = cellList;
	}
	
}

package sudoku;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import views.Welcome;

public class Sudoku {

	public static List<Cell> listCell = new ArrayList<>();
	public static int n = 1;

	public static void main(String[] args) throws IOException {

		Welcome welcome = new Welcome();

		createCellList();
		affectArea();
		setStaticCells(3);
		showCells(welcome);
		solve(welcome);

		
	}

	private static void showCells(Welcome welcome) {
		for (Cell cell : listCell) {
			if (cell.getValue() != 0) {
				welcome.sudoku.setValueAt(cell.getValue(), cell.getLine(), cell.getColumn());
			}
		}
	}

	public static void setStaticCells(int grid) {
		if (grid == 1) {
			SetCellWithLineAndColumn(0, 0, 9);
			SetCellWithLineAndColumn(2, 0, 5);
			SetCellWithLineAndColumn(1, 5, 1);
			SetCellWithLineAndColumn(2, 5, 3);
			SetCellWithLineAndColumn(1, 8, 7);
			SetCellWithLineAndColumn(2, 8, 4);
			SetCellWithLineAndColumn(3, 2, 7);
			SetCellWithLineAndColumn(4, 2, 3);
			SetCellWithLineAndColumn(4, 3, 6);
			SetCellWithLineAndColumn(5, 3, 4);
			SetCellWithLineAndColumn(4, 5, 8);
			SetCellWithLineAndColumn(3, 6, 2);
			SetCellWithLineAndColumn(5, 6, 6);
			SetCellWithLineAndColumn(5, 7, 1);
			SetCellWithLineAndColumn(6, 1, 8);
			SetCellWithLineAndColumn(6, 2, 5);
			SetCellWithLineAndColumn(8, 1, 4);
			SetCellWithLineAndColumn(7, 3, 3);
			SetCellWithLineAndColumn(6, 4, 4);
			SetCellWithLineAndColumn(7, 4, 2);
			SetCellWithLineAndColumn(8, 4, 1);
			SetCellWithLineAndColumn(7, 7, 6);
			SetCellWithLineAndColumn(8, 7, 9);
		}

		if (grid == 2) {
			SetCellWithLineAndColumn(0, 4, 3);
			SetCellWithLineAndColumn(1, 4, 7);
			SetCellWithLineAndColumn(1, 6, 9);
			SetCellWithLineAndColumn(3, 2, 1);
			SetCellWithLineAndColumn(4, 2, 9);
			SetCellWithLineAndColumn(5, 2, 7);
			SetCellWithLineAndColumn(3, 5, 9);
			SetCellWithLineAndColumn(5, 4, 4);
			SetCellWithLineAndColumn(5, 8, 3);
			SetCellWithLineAndColumn(5, 7, 9);
			SetCellWithLineAndColumn(6, 0, 7);
			SetCellWithLineAndColumn(6, 2, 2);
			SetCellWithLineAndColumn(8, 2, 4);
			SetCellWithLineAndColumn(8, 5, 3);
		}

		if (grid == 3) {
			SetCellWithLineAndColumn(0, 0, 8);
			SetCellWithLineAndColumn(0, 3, 4);
			SetCellWithLineAndColumn(0, 5, 5);
			SetCellWithLineAndColumn(0, 6, 9);
			SetCellWithLineAndColumn(0, 8, 7);
			SetCellWithLineAndColumn(1, 0, 5);
			SetCellWithLineAndColumn(1, 2, 9);
			SetCellWithLineAndColumn(1, 3, 2);
			SetCellWithLineAndColumn(1, 4, 3);
			SetCellWithLineAndColumn(2, 3, 7);
			SetCellWithLineAndColumn(3, 0, 2);
			SetCellWithLineAndColumn(3, 3, 1);
			SetCellWithLineAndColumn(3, 6, 3);
			SetCellWithLineAndColumn(4, 0, 7);
			SetCellWithLineAndColumn(4, 3, 8);
			SetCellWithLineAndColumn(5, 8, 4);
			SetCellWithLineAndColumn(6, 0, 3);
			SetCellWithLineAndColumn(6, 2, 2);
			SetCellWithLineAndColumn(6, 5, 8);
			SetCellWithLineAndColumn(6, 7, 9);
			SetCellWithLineAndColumn(7, 1, 8);
			SetCellWithLineAndColumn(7, 4, 7);
			SetCellWithLineAndColumn(7, 6, 2);
			SetCellWithLineAndColumn(7, 8, 3);
			SetCellWithLineAndColumn(8, 0, 9);
			SetCellWithLineAndColumn(8, 5, 2);
			SetCellWithLineAndColumn(8, 7, 4);
			SetCellWithLineAndColumn(8, 8, 5);
		}

		if (grid == 4) {
			SetCellWithLineAndColumn(0, 3, 5);
			SetCellWithLineAndColumn(1, 0, 1);
			SetCellWithLineAndColumn(1, 2, 6);
			SetCellWithLineAndColumn(1, 3, 8);
			SetCellWithLineAndColumn(1, 6, 4);
			SetCellWithLineAndColumn(1, 7, 5);
			SetCellWithLineAndColumn(2, 4, 4);
			SetCellWithLineAndColumn(3, 2, 7);
			SetCellWithLineAndColumn(3, 4, 8);
			SetCellWithLineAndColumn(3, 6, 6);
			SetCellWithLineAndColumn(3, 7, 4);
			SetCellWithLineAndColumn(4, 2, 4);
			SetCellWithLineAndColumn(4, 5, 6);
			SetCellWithLineAndColumn(4, 6, 5);
			SetCellWithLineAndColumn(4, 7, 1);
			SetCellWithLineAndColumn(5, 1, 5);
			SetCellWithLineAndColumn(5, 2, 3);
			SetCellWithLineAndColumn(5, 6, 8);
			SetCellWithLineAndColumn(5, 8, 9);
			SetCellWithLineAndColumn(6, 2, 5);
			SetCellWithLineAndColumn(6, 3, 4);
			SetCellWithLineAndColumn(6, 5, 7);
			SetCellWithLineAndColumn(6, 6, 3);
			SetCellWithLineAndColumn(7, 1, 3);
			SetCellWithLineAndColumn(7, 4, 9);

		}
	}

	public static void affectArea() {

		int i = 0;
		int area = 0;
		for (Cell cell : listCell) {

			cell.setArea(area);

			if (((i + 1) % 3 == 0) && (!((i + 1) % 9 == 0) || ((i + 1) % 27 == 0))) {
				area++;
			}

			if (((i + 1) % 9 == 0) && !((i + 1) % 27 == 0)) {
				area = area - 2;
			}

			i++;

			System.out.println("Line : " + cell.getLine() + " Column : " + cell.getColumn() + " Index : "
					+ listCell.indexOf(cell) + " Area : " + cell.getArea());
		}

	}

	public static void createCellList() {
		for (int line = 0; line < (9); line++) {

			for (int column = 0; column < (9); column++) {

				Cell cell = new Cell(line, column);
				listCell.add(cell);
			}

		}

	}

	public static Cell getCellWithLineAndColumn(int line, int column) {

		int i = 0;
		int n = 0;

		for (Cell cell : listCell) {
			i++;

			if ((cell.getLine() == line) && (cell.getColumn() == column)) {
				n = (i - 1);

			}
		}

		return listCell.get(n);

	}

	public static void SetCellWithLineAndColumn(int line, int column, int value) {

		getCellWithLineAndColumn(line, column).setValue(value);
		getCellWithLineAndColumn(line, column).setChangeableValue(false);

	}

	public static void solve(Welcome welcome) {

		Process process = new Process();
		process.index = 0;
		process.goForward = true;
		process.resolvable = true;

		while (process.index < 81 & process.resolvable == true) {

			solveCell(process, welcome);

		}

	}

	public static void solveCell(Process process, Welcome welcome) {
		if (process.index < 81) {
			Cell cell = listCell.get(process.index);
			if (cell.isChangeableValue()) {
				if (cell.getValue() == 9) {
					welcome.sudoku.setValueAt("", cell.getLine(), cell.getColumn());
					cell.setValue(0);
					process.index--;
					process.goForward = false;

				} else {
					cell.setValue(cell.getValue() + 1);

					int value = cell.getValue();
					welcome.sudoku.setValueAt(cell.getValue(), cell.getLine(), cell.getColumn());

					switch (value) {
					case 9:
						if (valueIsValid(cell)) {

							process.index++;
							process.goForward = true;

						} else {
							welcome.sudoku.setValueAt(0, cell.getLine(), cell.getColumn());
							cell.setValue(0);
							process.index--;
							process.goForward = false;

						}
						break;
					default:
						if (valueIsValid(cell)) {
							process.index++;
							process.goForward = true;

						}
						break;
					}
				}
			} else if (process.goForward == true) {
				process.index++;
				process.goForward = true;

			} else {
				process.index--;
				process.goForward = false;
			}
		}
	}

	public static void solveIt(int h, Welcome welcome, boolean goForward) {
		if (h < 81) {
			Cell cell = listCell.get(h);
			if (cell.isChangeableValue()) {
				if (cell.getValue() == 9) {
					// welcome.sudoku.setValueAt(0, cell.getLine(), cell.getColumn());
					cell.setValue(0);
					solveIt(h - 1, welcome, false);
				} else {
					cell.setValue(cell.getValue() + 1);

					int value = cell.getValue();
					// welcome.sudoku.setValueAt(cell.getValue(), cell.getLine(), cell.getColumn());

					switch (value) {
					case 9:
						if (valueIsValid(cell)) {

							solveIt(h + 1, welcome, true);

						} else {
							// welcome.sudoku.setValueAt(0, cell.getLine(), cell.getColumn());
							cell.setValue(0);
							solveIt(h - 1, welcome, false);
						}
						break;
					default:
						if (valueIsValid(cell)) {
							solveIt(h + 1, welcome, true);
						} else {
							solveIt(h, welcome, true);
						}
						break;
					}
				}
			} else if (goForward == true) {
				solveIt((h + 1), welcome, true);
			} else {
				solveIt((h - 1), welcome, false);
			}
		}
	}

	private static boolean valueIsValid(Cell cell) {
		if (!lineHasValue(cell) && !columnHasValue(cell) && !areaHasValue(cell)) {
			return true;
		} else
			return false;
	}

	private static boolean lineHasValue(Cell cell) {
		boolean hasValue = false;
		int value = cell.getValue();

		for (Cell cellOfList : listCell) {
			if (cell.getValue() != 0 && cell.getLine() == cellOfList.getLine() && (cellOfList.getValue() == value)
					&& (!cellOfList.equals(cell))) {

				hasValue = true;

			}

		}

		return hasValue;
	}

	private static boolean columnHasValue(Cell cell) {
		boolean hasValue = false;
		int value = cell.getValue();
		for (Cell cellOfList : listCell) {
			if (cell.getValue() != 0 && cell.getColumn() == cellOfList.getColumn() && (cellOfList.getValue() == value)
					&& (!cellOfList.equals(cell))) {

				hasValue = true;

			}

		}

		return hasValue;

	}

	private static boolean areaHasValue(Cell cell) {
		boolean hasValue = false;
		int value = cell.getValue();
		for (Cell cellOfList : listCell) {
			if (cell.getValue() != 0 && cell.getArea() == cellOfList.getArea() && (cellOfList.getValue() == value)
					&& (!cellOfList.equals(cell))) {
				hasValue = true;
			}
		}
		return hasValue;
	}

}

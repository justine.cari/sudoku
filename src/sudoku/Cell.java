package sudoku;

public class Cell {

	private int area;
	private int line;
	private int column;
	
	private int value;
	private boolean changeableValue;
	
	
	
	
	
	
	public Cell(int line, int column, int value, boolean changeableValue) {
		this.line = line;
		this.column = column;
		this.value = value;
		this.changeableValue = changeableValue;
		
	}
	
	public Cell(int line, int column) {
		this.line = line;
		this.column = column;
		this.area = -1;
		this.changeableValue=true;
	}
	
	public int getArea() {
		return area;
	}
	
	public void setArea(int area) {
		this.area=area;
			
		}
		
		
	public int getLine() {
		return line;
	}
	public void setLine(int line) {
		this.line = line;
	}
	public int getColumn() {
		return column;
	}
	public void setColumn(int column) {
		this.column = column;
	}
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	
	public boolean isChangeableValue() {
		return changeableValue;
	}
	public void setChangeableValue(boolean changeableValue) {
		this.changeableValue = changeableValue;
	}
	
	
	
	
	
}

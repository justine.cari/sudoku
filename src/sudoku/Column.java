package sudoku;

import java.util.ArrayList;
import java.util.List;

public class Column {
	
	private int index;
	private List<Cell> cellList = new ArrayList<>();
	
	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public List<Cell> getCellList() {
		return cellList;
	}

	public void setCellList(List<Cell> cellList) {
		this.cellList = cellList;
	}

	public Column (int index, List<Cell> cellList)
	{
		this.index = index;
		
		for (Cell cell : cellList) {
			if (cell.getColumn() == index)
			{
				this.cellList.add(cell);
			}
		}				
	}
}

package views;

import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableCellRenderer;


public class Welcome extends JFrame {

	private static final long serialVersionUID = 1L;
	
	
	public JTable sudoku = new JTable(9, 9);
	private DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
	

	public Welcome() throws IOException {
	
		
		
		
		
		
		JPanel panel = new JPanel();
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setTitle("Sudoku");
		setName("Sudoku");
		setResizable(false);
		setBounds(300, 300, 300, 300);
		cellRenderer.setHorizontalAlignment(JLabel.CENTER);		
		for (int i=0 ; i<sudoku.getColumnCount() ; i++) 
		{  sudoku.getColumnModel().getColumn(i).setCellRenderer(cellRenderer); 
			   }
	
		sudoku.setDefaultRenderer(getClass(), cellRenderer);
		sudoku.setRowHeight(50);
		sudoku.setOpaque(false);
		
			
		
		getContentPane().add(sudoku);
		
	
	    pack();
				
		setVisible(true);
		
		
	}
	
	
}
